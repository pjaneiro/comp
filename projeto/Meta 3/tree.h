#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

typedef enum {programNode, varPartNode, funcPartNode, varDeclNode, funcDeclNode, funcDefNode, funcDef2Node, funcParamsNode, paramsNode, varParamsNode, assignNode, ifElseNode, repeatNode, statListNode, valParamNode, whileNode, writeLnNode, addNode, andNode, callNode, divNode, eqNode, geqNode, gtNode, leqNode, ltNode, minusNode, modNode, mulNode, neqNode, notNode, orNode, plusNode, realDivNode, subNode, idNode, intLitNode, realLitNode, stringNode, unknownNode} node_type_t;
typedef enum {intVar, realVar, idVar, stringVar, booleanVar, unknownVar, opVar, funcVar, typeVar} var_type_t;
typedef struct node
{
    node_type_t nodeType;
    char* nodeTypeString;
    var_type_t varType;
    char* var;
    int line;
    int column;
    struct node** children;
    int nChildren;
    struct node* parent;
    struct node* left;
    struct node* right;
} node;
typedef struct data
{
	char* val;
	int line;
	int column;
} data;

node* createNode(node_type_t, char*, var_type_t, data);
void addChild(node*, node*);
void addSibling(node*, node*);
void printTree(node*, int);
void clearTree(node*);

#define OP (data){(char*)calloc(1,sizeof(char)),0,0}
node* root;

#endif // TREE_H_INCLUDED
