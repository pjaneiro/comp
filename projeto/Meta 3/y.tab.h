/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ASSIGNSYM = 258,
    BEGINSYM = 259,
    COLONSYM = 260,
    COMMASYM = 261,
    DOSYM = 262,
    DOTSYM = 263,
    ELSESYM = 264,
    ENDSYM = 265,
    FORWARDSYM = 266,
    FUNCTIONSYM = 267,
    IFSYM = 268,
    LBRACSYM = 269,
    NOTSYM = 270,
    OUTPUTSYM = 271,
    PARAMSTRSYM = 272,
    PROGRAMSYM = 273,
    RBRACSYM = 274,
    REPEATSYM = 275,
    SEMICSYM = 276,
    THENSYM = 277,
    UNTILSYM = 278,
    VALSYM = 279,
    VARSYM = 280,
    WHILESYM = 281,
    WRITELNSYM = 282,
    RESERVED = 283,
    ID = 284,
    STRING = 285,
    INTLIT = 286,
    REALLIT = 287,
    LT = 288,
    GT = 289,
    EQ = 290,
    NEQ = 291,
    LEQ = 292,
    GEQ = 293,
    PLUS = 294,
    MINUS = 295,
    OR = 296,
    MUL = 297,
    REALDIV = 298,
    MOD = 299,
    DIV = 300,
    AND = 301
  };
#endif
/* Tokens.  */
#define ASSIGNSYM 258
#define BEGINSYM 259
#define COLONSYM 260
#define COMMASYM 261
#define DOSYM 262
#define DOTSYM 263
#define ELSESYM 264
#define ENDSYM 265
#define FORWARDSYM 266
#define FUNCTIONSYM 267
#define IFSYM 268
#define LBRACSYM 269
#define NOTSYM 270
#define OUTPUTSYM 271
#define PARAMSTRSYM 272
#define PROGRAMSYM 273
#define RBRACSYM 274
#define REPEATSYM 275
#define SEMICSYM 276
#define THENSYM 277
#define UNTILSYM 278
#define VALSYM 279
#define VARSYM 280
#define WHILESYM 281
#define WRITELNSYM 282
#define RESERVED 283
#define ID 284
#define STRING 285
#define INTLIT 286
#define REALLIT 287
#define LT 288
#define GT 289
#define EQ 290
#define NEQ 291
#define LEQ 292
#define GEQ 293
#define PLUS 294
#define MINUS 295
#define OR 296
#define MUL 297
#define REALDIV 298
#define MOD 299
#define DIV 300
#define AND 301

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 29 "mpasemantic.y" /* yacc.c:1909  */

    struct node* no;
    struct data val;

#line 151 "y.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
