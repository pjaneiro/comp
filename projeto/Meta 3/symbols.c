#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include "tree.h"
#include "symbols.h"

int semLine = INT_MAX;
int semColumn = INT_MAX;
char* semError = 0;
//typedef enum {intVar, realVar, idVar, stringVar, booleanVar, unknownVar, opVar, funcVar, typeVar} var_type_t;
char* printableTypes[] = {"_integer_","_real_","_id_","_string_","_boolean_","_unknown_","_unknown_","_function_","_type_"};

symbol* createSymbol(char* name, symbol_type_t type, flag_type_t flag, char* value, int line, int column)
{
    symbol* result = (symbol*)malloc(sizeof(symbol));
    result->name = (char*)calloc(1+strlen(name),sizeof(char));
    strcpy(result->name, name);
    result->type = type;
    result->flag = flag;
    result->value = (char*)calloc(1+strlen(value),sizeof(char));
    strcpy(result->value, value);
    result->line = line;
    result->column = column;
    return result;
}

table* createTable(table_type_t type)
{
    table* result = (table*)malloc(sizeof(table));
    result->type = type;
    result->next = 0;
    result->previous = 0;
    result->symbols = 0;
    result->nSymbols = 0;
    return result;
}

void addSymbolToTable(table* where, symbol* what)
{
    where->nSymbols++;
    where->symbols = (symbol**)realloc(where->symbols, where->nSymbols*sizeof(symbol*));
    where->symbols[where->nSymbols-1] = what;
}

table* createAllTables()
{
    if(root == 0)
    {
        return 0;
    }
    int i, j, k, l;
    table* result;
    table* current;
    table* program;
    current = createTable(outerTable);
    result = current;
    addSymbolToTable(current, createSymbol("boolean",   typeSymbol,     constantFlag,   "_boolean_", -1, -1));
    addSymbolToTable(current, createSymbol("integer",   typeSymbol,     constantFlag,   "_integer_", -1, -1));
    addSymbolToTable(current, createSymbol("real",      typeSymbol,     constantFlag,   "_real_", -1, -1));
    addSymbolToTable(current, createSymbol("false",     booleanSymbol,  constantFlag,   "_false_", -1, -1));
    addSymbolToTable(current, createSymbol("true",      booleanSymbol,  constantFlag,   "_true_", -1, -1));
    addSymbolToTable(current, createSymbol("paramcount",functionSymbol, noFlag,         "", -1, -1));
    addSymbolToTable(current, createSymbol("program",   programSymbol,  noFlag,         "", -1, -1));
    current->next = createTable(functionTable);
    current->next->previous = current;
    current = current->next;
    addSymbolToTable(current, createSymbol("paramcount", integerSymbol, returnFlag, "", -1, -1));
    current->next = createTable(programTable);
    current->next->previous = current;
    current = current->next;
    program = current;
    node* varPart = root->children[1];
    for(i=0; i<varPart->nChildren; i++)
    {
        node* varDecl = varPart->children[i];
        for(j=0; j<varDecl->nChildren-1; j++)
        {
            addSymbolToTable(program, createSymbol(stringToLower(varDecl->children[j]->var), symbolFromString(varDecl->children[varDecl->nChildren-1]->var), noFlag,  "", varDecl->children[j]->line, varDecl->children[j]->column));
        }
    }
    node* funcPart = root->children[2];
    for(i=0; i<funcPart->nChildren; i++)
    {
        node* func = funcPart->children[i];
        node* funcParams;
        switch(func->nodeType)
        {
        case funcDeclNode:
            addSymbolToTable(program, createSymbol(stringToLower(func->children[0]->var), functionSymbol, noFlag, "", func->children[0]->line, func->children[0]->column));
            current->next = createTable(functionTable);
            current->next->previous = current;
            current = current->next;
            addSymbolToTable(current, createSymbol(stringToLower(func->children[0]->var), symbolFromString(func->children[2]->var), returnFlag, "", func->children[0]->line, func->children[0]->column));
            funcParams = func->children[1];
            for(j=0; j<funcParams->nChildren; j++)
            {
                node* params = funcParams->children[j];
                if(params->nodeType == paramsNode)
                {
                    for(k=0; k<params->nChildren-1; k++)
                    {
                        addSymbolToTable(current, createSymbol(stringToLower(params->children[k]->var), symbolFromString(params->children[params->nChildren-1]->var), paramFlag, "", params->children[k]->line, params->children[k]->column));
                    }
                }
                else if(params->nodeType == varParamsNode)
                {
                    for(k=0; k<params->nChildren-1; k++)
                    {
                        addSymbolToTable(current, createSymbol(stringToLower(params->children[k]->var), symbolFromString(params->children[params->nChildren-1]->var), varParamFlag, "", params->children[k]->line, params->children[k]->column));
                    }
                }
            }
            break;
        case funcDefNode:
            addSymbolToTable(program, createSymbol(stringToLower(func->children[0]->var), functionSymbol, noFlag, "", func->children[0]->line, func->children[0]->column));
            current->next = createTable(functionTable);
            current->next->previous = current;
            current = current->next;
            addSymbolToTable(current, createSymbol(stringToLower(func->children[0]->var), symbolFromString(func->children[2]->var), returnFlag, "", func->children[0]->line, func->children[0]->column));
            funcParams = func->children[1];
            for(j=0; j<funcParams->nChildren; j++)
            {
                node* params = funcParams->children[j];
                if(params->nodeType == paramsNode)
                {
                    for(k=0; k<params->nChildren-1; k++)
                    {
                        addSymbolToTable(current, createSymbol(stringToLower(params->children[k]->var), symbolFromString(params->children[params->nChildren-1]->var), paramFlag, "", params->children[k]->line, params->children[k]->column));
                    }
                }
                else if(params->nodeType == varParamsNode)
                {
                    for(k=0; k<params->nChildren-1; k++)
                    {
                        for(l=0; l<current->nSymbols; l++)
                        {
                            if(strcmp(stringToLower(current->symbols[l]->name), stringToLower(params->children[k]->var)) == 0)
                            {
                                char* newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Symbol %s already defined", params->children[k]->var);
                                addError(newError, params->children[k]->line, params->children[k]->column);
                                return result;
                            }
                        }
                        addSymbolToTable(current, createSymbol(stringToLower(params->children[k]->var), symbolFromString(params->children[params->nChildren-1]->var), varParamFlag, "", params->children[k]->line, params->children[k]->column));
                    }
                }
            }
            varPart = func->children[3];
            for(j=0; j<varPart->nChildren; j++)
            {
                node* varDecl = varPart->children[j];
                for(k=0; k<varDecl->nChildren-1; k++)
                {
                    addSymbolToTable(current, createSymbol(stringToLower(varDecl->children[k]->var), symbolFromString(varDecl->children[varDecl->nChildren-1]->var), noFlag, "", varDecl->children[k]->line, varDecl->children[k]->column));
                }
            }
            break;
        case funcDef2Node:
            current = program->next;
            while(current!=0)
            {
                if(strcmp(current->symbols[0]->name, stringToLower(func->children[0]->var)) == 0)
                {
                    break;
                }
                current = current->next;
            }
            if(current!=0)
            {
                varPart = func->children[1];
                for(j=0; j<varPart->nChildren; j++)
                {
                    node* varDecl = varPart->children[j];
                    for(k=0; k<varDecl->nChildren-1; k++)
                    {
                        addSymbolToTable(current, createSymbol(stringToLower(varDecl->children[k]->var), symbolFromString(varDecl->children[varDecl->nChildren-1]->var), noFlag, "", varDecl->children[k]->line, varDecl->children[k]->column));
                    }
                }
            }
            current = program;
            while(current->next != 0)
            {
                current = current->next;
            }
            break;
        default:
            break;
        }
    }

    return result;
}

void printTables(table* current)
{
    int i;
    while(current!=0)
    {
        switch(current->type)
        {
        case outerTable:
            printf("===== Outer Symbol Table =====\n");
            break;
        case programTable:
            printf("===== Program Symbol Table =====\n");
            break;
        case functionTable:
            printf("===== Function Symbol Table =====\n");
            break;
        }
        for(i=0; i<current->nSymbols; i++)
        {
            printSymbol(current->symbols[i]);
        }
        current = current->next;
        if(current!=0)
        {
            printf("\n");
        }
    }
}

void printSymbol(symbol* toPrint)
{
    if(toPrint==0)
    {
        return;
    }
    printf("%s", toPrint->name);
    switch(toPrint->type)
    {
    case booleanSymbol:
        printf("\t_boolean_");
        break;
    case integerSymbol:
        printf("\t_integer_");
        break;
    case realSymbol:
        printf("\t_real_");
        break;
    case functionSymbol:
        printf("\t_function_");
        break;
    case programSymbol:
        printf("\t_program_");
        break;
    case typeSymbol:
        printf("\t_type_");
        break;
    case noSymbol:
        printf("\t_error_");
        break;
    }
    switch(toPrint->flag)
    {
    case constantFlag:
        printf("\tconstant");
        break;
    case returnFlag:
        printf("\treturn");
        break;
    case paramFlag:
        printf("\tparam");
        break;
    case varParamFlag:
        printf("\tvarparam");
        break;
    default:
        break;
    }
    if(strcmp(toPrint->value,"")!=0)
    {
        printf("\t%s", toPrint->value);
    }
    printf("\n");
}

char* stringToLower(char* string)
{
    char* result = (char*)calloc(1+strlen(string), sizeof(char));
    int i;
    for(i=0; i<strlen(string); i++)
    {
        result[i] = tolower(string[i]);
    }
    return result;
}

symbol_type_t symbolFromString(char* string)
{
    if(strcmp(stringToLower(string), "integer") == 0)
    {
        return integerSymbol;
    }
    else if(strcmp(stringToLower(string), "real") == 0)
    {
        return realSymbol;
    }
    else if(strcmp(stringToLower(string), "boolean") == 0)
    {
        return booleanSymbol;
    }
    else
    {
        return noSymbol;
    }
}

void clearTables(table* current)
{
    if(current == 0)
    {
        return;
    }
    clearTables(current->next);
    int i;
    for(i=0; i<current->nSymbols; i++)
    {
        free(current->symbols[i]->name);
        free(current->symbols[i]->value);
        free(current->symbols[i]);
    }
    free(current->symbols);
    free(current);
    return;
}

void addError(char* newError, int line, int column)
{
    if(semError != 0)
    {
        free(newError);
        return;
    }
    semError = newError;
    semLine = line;
    semColumn = column;
}

int semanticCheck(node* current, char* scope)
{
    if(current == 0)
    {
        return 0;
    }
    int i, j, sum=0, pass=0;
    char* comp1;
    char* comp2;
    char* newError;
    table* aux;
    switch(current->nodeType)
    {
        case programNode:
            for(i=1; i<current->nChildren; i++)
            {
                sum+=semanticCheck(current->children[i], scope);
            }
            return sum;
        case varPartNode:
            for(i=0; i<current->nChildren; i++)
            {
                sum+=semanticCheck(current->children[i], scope);
            }
            return sum;
        case funcDeclNode:
        case funcDefNode:
            aux = start->next->next->next;
            while(aux!=0)
            {
                comp1 = stringToLower(aux->symbols[0]->name);
                comp2 = stringToLower(current->children[0]->var);
                if(strcmp(comp1, comp2) == 0 && (aux->symbols[0]->line < current->children[0]->line || (aux->symbols[0]->line == current->children[0]->line && aux->symbols[0]->column < current->children[0]->column)))
                {
                    free(comp1);
                    free(comp2);
                    newError = (char*)calloc(1024, sizeof(char));
                    sprintf(newError, "Symbol %s already defined", current->children[0]->var);
                    addError(newError, current->children[0]->line, current->children[0]->column);
                    return 1;
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
                aux = aux->next;
            }
            for(i=1; i<current->nChildren; i++)
            {
                sum+=semanticCheck(current->children[i], current->children[0]->var);
            }
            return sum; 
        case funcDef2Node:
            ;
            node* parent = current->parent;
            {
                for(i=0; i<parent->nChildren; i++)
                {
                    if(parent->children[i]->nodeType==funcDefNode || parent->children[i]->nodeType==funcDef2Node)
                    {
                        comp1 = stringToLower(parent->children[i]->children[0]->var);
                        comp2 = stringToLower(current->children[0]->var);
                        if(strcmp(comp1, comp2) == 0 && (parent->children[i]->children[0]->line < current->children[0]->line || (parent->children[i]->children[0]->line == current->children[0]->line && parent->children[i]->children[0]->column < current->children[0]->column)))
                        {
                            free(comp1);
                            free(comp2);
                            newError = (char*)calloc(1024, sizeof(char));
                            sprintf(newError, "Symbol %s already defined", current->children[0]->var);
                            addError(newError, current->children[0]->line, current->children[0]->column);
                            return 1;
                        }
                        else
                        {
                            free(comp1);
                            free(comp2);
                        }
                    }
                }
            }
            aux = start->next->next;
            pass = 0;
            for(i=0; i<aux->nSymbols; i++)
            {
                comp1 = stringToLower(aux->symbols[i]->name);
                comp2 = stringToLower(current->children[0]->var);
                if(strcmp(comp1, comp2) == 0 && aux->symbols[i]->type == functionSymbol)
                {
                    free(comp1);
                    free(comp2);
                    pass = 1;
                    break;
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
            }
            if(!pass)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Function identifier expected");
                addError(newError, current->children[0]->line, current->children[0]->column);
                return 1;
            }
            for(i=1; i<current->nChildren; i++)
            {
                sum+=semanticCheck(current->children[i], current->children[0]->var);
            }
            return sum;
        case idNode:
            ;
            aux = start->next->next->next;
            while(aux!=0)
            {
                comp1 = stringToLower(aux->symbols[0]->name);
                comp2 = stringToLower(scope);
                if(strcmp(comp1, comp2) == 0)
                {
                    free(comp1);
                    free(comp2);
                    for(i=0; i<aux->nSymbols; i++)
                    {
                        comp1 = stringToLower(aux->symbols[i]->name);
                        comp2 = stringToLower(current->var);
                        if(strcmp(comp1, comp2) == 0)
                        {
                            free(comp1);
                            free(comp2);
                            switch(aux->symbols[i]->type)
                            {
                                case(booleanSymbol):
                                {
                                    current->varType = booleanVar;
                                    return 0;
                                }
                                case(integerSymbol):
                                {
                                    current->varType = intVar;
                                    return 0;
                                }
                                case(realSymbol):
                                {
                                    current->varType = realVar;
                                    return 0;
                                }
                                case(functionSymbol):
                                {
                                    current->varType = funcVar;
                                    return 0;
                                }
                                case(typeSymbol):
                                {
                                    current->varType = typeVar;
                                    return 0;
                                }
                                default:
                                {
                                    current->varType = unknownVar;
                                    return 1;
                                }
                            }
                            break;
                        }
                        else
                        {
                            free(comp1);
                            free(comp2);
                        }
                    }
                    break;
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
                aux = aux->next;
            }
            if(current->varType == idVar)
            {
                aux = start->next->next;
                for(i=0; i<aux->nSymbols; i++)
                {
                    comp1 = stringToLower(aux->symbols[i]->name);
                    comp2 = stringToLower(current->var);
                    if(strcmp(comp1, comp2) == 0)
                    {
                        free(comp1);
                        free(comp2);
                        switch(aux->symbols[i]->type)
                        {
                            case(booleanSymbol):
                            {
                                current->varType = booleanVar;
                                return 0;
                            }
                            case(integerSymbol):
                            {
                                current->varType = intVar;
                                return 0;
                            }
                            case(realSymbol):
                            {
                                current->varType = realVar;
                                return 0;
                            }
                            case(functionSymbol):
                            {
                                aux = start->next->next->next;
                                while(aux!=0)
                                {
                                    comp1 = stringToLower(aux->symbols[0]->name);
                                    comp2 = stringToLower(current->var);
                                    if(strcmp(comp1, comp2) == 0)
                                    {
                                        current->nodeType = unknownNode;
                                        free(comp1);
                                        free(comp2);
                                        switch(aux->symbols[0]->type)
                                        {
                                            case(booleanSymbol):
                                            {
                                                current->varType = booleanVar;
                                                return 0;
                                            }
                                            case(integerSymbol):
                                            {
                                                current->varType = intVar;
                                                return 0;
                                            }
                                            case(realSymbol):
                                            {
                                                current->varType = realVar;
                                                return 0;
                                            }
                                            case(typeSymbol):
                                            {
                                                current->varType = typeVar;
                                                return 0;
                                            }
                                            default:
                                            {
                                                current->varType = unknownVar;
                                                break;
                                            }
                                        }
                                        return 0;
                                    }
                                    else
                                    {
                                        free(comp1);
                                        free(comp2);
                                    }
                                    aux = aux->next;
                                }
                                return 0;
                            }
                            case(typeSymbol):
                            {
                                current->varType = typeVar;
                                return 0;
                            }
                            default:
                            {
                                current->varType = unknownVar;
                                return 1;
                            }
                        }
                        break;
                    }
                    else
                    {
                        free(comp1);
                        free(comp2);
                    }
                }
            }
            if(current->varType == idVar)
            {
                aux = start;
                for(i=0; i<aux->nSymbols; i++)
                {
                    comp1 = stringToLower(aux->symbols[i]->name);
                    comp2 = stringToLower(current->var);
                    if(strcmp(comp1, comp2) == 0)
                    {
                        free(comp1);
                        free(comp2);
                        switch(aux->symbols[i]->type)
                        {
                            case(booleanSymbol):
                            {
                                current->varType = booleanVar;
                                return 0;
                            }
                            case(integerSymbol):
                            {
                                current->varType = intVar;
                                return 0;
                            }
                            case(realSymbol):
                            {
                                current->varType = realVar;
                                return 0;
                            }
                            case(functionSymbol):
                            {
                                comp1 = stringToLower("paramcount");
                                comp2 = stringToLower(current->var);
                                if(strcmp(comp1, comp2) == 0)
                                {
                                    free(comp1);
                                    free(comp2);
                                    current->nodeType = unknownNode;
                                    current->varType = intVar;
                                    return 0;
                                }
                                else
                                {
                                    free(comp1);
                                    free(comp2);
                                    return 1;
                                }
                            }
                            case(typeSymbol):
                            {
                                current->varType = typeVar;
                                return 0;
                            }
                            default:
                            {
                                current->varType = unknownVar;
                                return 1;
                            }
                        }
                        break;
                    }
                    else
                    {
                        free(comp1);
                        free(comp2);
                    }
                }
            }
            if(current->varType == idVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Symbol %s not defined", current->var);
                addError(newError, current->line, current->column);
                return 1;
            }
            return 0;
        case paramsNode:
        case varParamsNode:
        case varDeclNode:
            aux = start;
            int existant = 0;
            for(i=0; i<aux->nSymbols; i++)
            {
                comp1 = stringToLower(aux->symbols[i]->name);
                comp2 = stringToLower(current->children[current->nChildren-1]->var);
                if(strcmp(comp1, comp2) == 0)
                {
                    free(comp1);
                    free(comp2);
                    if(aux->symbols[i]->type!=typeSymbol)
                    {
                        newError = (char*)calloc(1024, sizeof(char));
                        sprintf(newError, "Type identifier expected");
                        addError(newError, current->children[current->nChildren-1]->line, current->children[current->nChildren-1]->column);
                        return 1;
                    }
                    else
                    {
                        existant = 1;
                    }
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
            }
            aux = aux->next->next;
            for(i=0; i<aux->nSymbols; i++)  //Program table, para cada símbolo
            {
                pass = 0;
                for(j=0; j<current->nChildren-1; j++)   //Para cada filho do nó atual
                {
                    if(current->children[j]->line < aux->symbols[i]->line || (current->children[j]->line == aux->symbols[i]->line && current->children[j]->column < aux->symbols[i]->column))
                    {
                        pass = 1;
                        break;
                    }
                    else    //Se símbolo definido depois do filho atual
                    {
                        comp1 = stringToLower(aux->symbols[i]->name);
                        comp2 = stringToLower(current->children[current->nChildren-1]->var);
                        if(strcmp(comp1, comp2) == 0)
                        {
                            free(comp1);
                            free(comp2);
                            if(aux->symbols[i]->type!=typeSymbol)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Type identifier expected");
                                addError(newError, current->children[current->nChildren-1]->line, current->children[current->nChildren-1]->column);
                                return 1;
                            }
                            else
                            {
                                existant = 1;
                            }
                        }
                        else
                        {
                            free(comp1);
                            free(comp2);
                        }
                    }
                }
                if(pass)
                {
                    break;
                }
            }
            aux = aux->next;
            while(aux!=0)
            {
                comp1 = stringToLower(aux->symbols[0]->name);
                comp2 = stringToLower(scope);
                if(strcmp(comp1, comp2) == 0)
                {
                    free(comp1);
                    free(comp2);
                    for(i=0; i<aux->nSymbols; i++)
                    {
                        pass = 0;
                        for(j=0; j<current->nChildren-1; j++)
                        {
                            if(current->children[j]->line < aux->symbols[i]->line || (current->children[j]->line == aux->symbols[i]->line && current->children[j]->column < aux->symbols[i]->column))
                            {
                                pass = 1;
                                break;
                            }
                            else
                            {
                                if(strcmp(stringToLower(aux->symbols[i]->name), stringToLower(current->children[current->nChildren-1]->var)) == 0)
                                {
                                    if(aux->symbols[i]->type!=typeSymbol)
                                    {
                                        newError = (char*)calloc(1024, sizeof(char));
                                        sprintf(newError, "Type identifier expected");
                                        addError(newError, current->children[current->nChildren-1]->line, current->children[current->nChildren-1]->column);
                                        return 1;
                                    }
                                    else
                                    {
                                        existant = 1;
                                    }
                                }
                            }
                        } 
                        if(pass)
                        {
                            break;
                        }                       
                    }
                    break;
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
                aux = aux->next;
            }
            if(!existant)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Symbol %s not defined", current->children[current->nChildren-1]->var);
                addError(newError, current->children[current->nChildren-1]->line, current->children[current->nChildren-1]->column);
                return 1;
            }
            if(strcmp(scope, "") == 0)  //Se var de programa
            {
                aux = start->next->next;    //Program table
                for(i=0; i<aux->nSymbols; i++)   //Percorre os símbolos
                {
                    for(j=0; j<current->nChildren-1; j++)   //Percorre as variáveis declaradas
                    {
                        if(aux->symbols[i]->line < current->children[j]->line || (aux->symbols[i]->line == current->children[j]->line && aux->symbols[i]->column < current->children[j]->column))   //Se definido depois do atual
                        {
                            comp1 = stringToLower(aux->symbols[i]->name);
                            comp2 = stringToLower(current->children[j]->var);
                            if(strcmp(comp1, comp2) == 0)  //Redefinição
                            {
                                free(comp1);
                                free(comp2);
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Symbol %s already defined", current->children[j]->var);
                                addError(newError, current->children[j]->line, current->children[j]->column);
                                return 1;
                            }
                            else
                            {
                                free(comp1);
                                free(comp2);
                            }
                        }
                    }
                }
            }
            else
            {
                aux = start->next->next->next;
                while(aux!=0)
                {
                    comp1 = stringToLower(aux->symbols[0]->name);
                    comp2 = stringToLower(scope);
                    if(strcmp(comp1, comp2) == 0)
                    {
                        free(comp1);
                        free(comp2);
                        for(i=0; i<aux->nSymbols; i++)   //Percorre os símbolos
                        {
                            for(j=0; j<current->nChildren-1; j++)   //Percorre as variáveis declaradas
                            {
                                if(aux->symbols[i]->line < current->children[j]->line || (aux->symbols[i]->line == current->children[j]->line && aux->symbols[i]->column < current->children[j]->column))   //Se definido depois do atual
                                {
                                    if(strcmp(stringToLower(aux->symbols[i]->name), stringToLower(current->children[j]->var)) == 0)  //Redefinição
                                    {
                                        newError = (char*)calloc(1024, sizeof(char));
                                        sprintf(newError, "Symbol %s already defined", current->children[j]->var);
                                        addError(newError, current->children[j]->line, current->children[j]->column);
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        free(comp1);
                        free(comp2);
                    }
                    aux = aux->next;
                }
            }
            for(i=0; i<current->nChildren-1; i++)
            {
                sum+=semanticCheck(current->children[i], scope);
            }
            return sum;
        case ifElseNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[0]->varType != booleanVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in if statement (got %s, expected _boolean_)", printableTypes[current->children[0]->varType]);
                addError(newError, current->children[0]->line, current->children[0]->column);
                return 1;
            }
            if(current->children[1]->nodeType == statListNode && current->children[1]->nChildren != 0)
            {
                for(i=0; i<current->children[1]->nChildren; i++)
                {
                    sum+=semanticCheck(current->children[1]->children[i], scope);
                }
            }
            else if(current->children[1]->nodeType != statListNode)
            {
                sum+=semanticCheck(current->children[1], scope);
            }
            if(current->children[2]->nodeType == statListNode && current->children[2]->nChildren != 0)
            {
                for(i=0; i<current->children[2]->nChildren; i++)
                {
                    sum+=semanticCheck(current->children[2]->children[i], scope);
                }
            }
            else if(current->children[2]->nodeType != statListNode)
            {
                sum+=semanticCheck(current->children[2], scope);
            }
            return sum;
        case whileNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[0]->varType != booleanVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in while statement (got %s, expected _boolean_)", printableTypes[current->children[0]->varType]);
                addError(newError, current->children[0]->line, current->children[0]->column);
                return 1;
            }
            if(current->children[1]->nodeType == statListNode && current->children[1]->nChildren != 0)
            {
                for(i=0; i<current->children[1]->nChildren; i++)
                {
                    sum+=semanticCheck(current->children[1]->children[i], scope);
                }
            }
            else if(current->children[1]->nodeType != statListNode)
            {
                sum+=semanticCheck(current->children[1], scope);
            }
            return sum;
        case repeatNode:
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[1]->varType != booleanVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in repeat-until statement (got %s, expected _boolean_)", printableTypes[current->children[1]->varType]);
                addError(newError, current->children[1]->line, current->children[1]->column);
                return 1;
            }
            if(current->children[0]->nodeType == statListNode && current->children[0]->nChildren != 0)
            {
                for(i=0; i<current->children[0]->nChildren; i++)
                {
                    sum+=semanticCheck(current->children[0]->children[i], scope);
                }
            }
            else if(current->children[0]->nodeType != statListNode)
            {
                sum+=semanticCheck(current->children[0], scope);
            }
            return sum;
        case valParamNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[0]->varType != intVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in val-paramstr statement (got %s, expected _integer_)", printableTypes[current->children[0]->varType]);
                addError(newError, current->children[0]->line, current->children[0]->column);
                return 1;
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[1]->nodeType == unknownNode)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Variable identifier expected");
                addError(newError, current->children[1]->line, current->children[1]->column);
                return 1;
            }
            if(current->children[1]->varType != intVar)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in val-paramstr statement (got %s, expected _integer_)", printableTypes[current->children[1]->varType]);
                addError(newError, current->children[1]->line, current->children[1]->column);
                return 1;
            }
            return 0;
        case assignNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[0]->nodeType == unknownNode)
            {
                comp1 = stringToLower(current->children[0]->var);
                comp2 = stringToLower(scope);
                if(strcmp(comp1, comp2) != 0)
                {
                    newError = (char*)calloc(1024, sizeof(char));
                    sprintf(newError, "Variable identifier expected");
                    addError(newError, current->children[0]->line, current->children[0]->column);
                    return 1;
                }
            }
            if(current->children[1]->nodeType == unknownNode)
            {
                aux = start->next;
                while(aux!=0)
                {
                    if(aux->type == programTable)
                    {
                        aux = aux->next;
                        continue;
                    }
                    comp1 = stringToLower(aux->symbols[0]->name);
                    comp2 = stringToLower(current->children[1]->var);
                    if(strcmp(comp1, comp2) == 0)
                    {
                        free(comp1);
                        free(comp2);
                        int nParams = 0;
                        for(i=1; i<aux->nSymbols; i++)
                        {
                            if(aux->symbols[i]->flag == paramFlag || aux->symbols[i]->flag == varParamFlag)
                            {
                                nParams++;
                            }
                        }
                        if(nParams != 0)
                        {
                            newError = (char*)calloc(1024, sizeof(char));
                            sprintf(newError, "Wrong number of arguments in call to function %s (got 0, expected %d)",current->children[1]->var, nParams);
                            addError(newError, current->children[1]->line, current->children[1]->column);
                            return 1;
                        }
                    }
                    else
                    {
                        free(comp1);
                        free(comp2);
                    }
                    aux = aux->next;
                }

            }
            if((current->children[0]->varType == intVar && current->children[1]->varType == intVar) || (current->children[0]->varType == realVar && current->children[1]->varType == intVar) || (current->children[0]->varType == realVar && current->children[1]->varType == realVar))
            {
                return 0;
            }
            if(current->children[0]->varType != current->children[1]->varType)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Incompatible type in assigment to %s (got %s, expected %s)", current->children[0]->var, printableTypes[current->children[1]->varType], printableTypes[current->children[0]->varType]);
                addError(newError, current->children[1]->line, current->children[1]->column);
                return 1;
            }
        case intLitNode:
        case realLitNode:
            break;
        case addNode:
        case subNode:
        case mulNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[0]->varType == intVar && current->children[1]->varType == intVar)
            {
                current->varType = intVar;
                return 0;
            }
            else if((current->children[0]->varType == intVar && current->children[1]->varType == realVar) || (current->children[0]->varType == realVar && current->children[1]->varType == intVar) || (current->children[0]->varType == realVar && current->children[1]->varType == realVar))
            {
                current->varType = realVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to types %s, %s", current->var, printableTypes[current->children[0]->varType], printableTypes[current->children[1]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case realDivNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if((current->children[0]->varType == intVar || current->children[0]->varType == realVar) && (current->children[1]->varType == intVar || current->children[1]->varType == realVar))
            {
                current->varType = realVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to types %s, %s", current->var, printableTypes[current->children[0]->varType], printableTypes[current->children[1]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case modNode:
        case divNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[0]->varType == intVar && current->children[1]->varType == intVar)
            {
                current->varType = intVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to types %s, %s", current->var, printableTypes[current->children[0]->varType], printableTypes[current->children[1]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case ltNode:
        case gtNode:
        case leqNode:
        case geqNode:
        case eqNode:
        case neqNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(((current->children[0]->varType == intVar || current->children[0]->varType == realVar) && (current->children[1]->varType == intVar || current->children[1]->varType == realVar)) || (current->children[0]->varType == booleanVar && current->children[1]->varType == booleanVar))
            {
                current->varType = booleanVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to types %s, %s", current->var, printableTypes[current->children[0]->varType], printableTypes[current->children[1]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case plusNode:
        case minusNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[0]->varType == intVar)
            {
                current->varType = intVar;
                return 0;
            }
            else if(current->children[0]->varType == realVar)
            {
                current->varType = realVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to type %s", current->var, printableTypes[current->children[0]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case notNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[0]->varType == booleanVar)
            {
                current->varType = booleanVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to type %s", current->var, printableTypes[current->children[0]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case andNode:
        case orNode:
            if(current->children[0]->varType == idVar || current->children[0]->varType == unknownVar || current->children[0]->varType == opVar)
            {
                semanticCheck(current->children[0], scope);
            }
            if(current->children[1]->varType == idVar || current->children[1]->varType == unknownVar || current->children[1]->varType == opVar)
            {
                semanticCheck(current->children[1], scope);
            }
            if(current->children[0]->varType == booleanVar && current->children[1]->varType == booleanVar)
            {
                current->varType = booleanVar;
                return 0;
            }
            else
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Operator %s cannot be applied to types %s, %s", current->var, printableTypes[current->children[0]->varType], printableTypes[current->children[1]->varType]);
                addError(newError, current->line, current->column);
                return 1;
            }
            break;
        case callNode:
            aux = start->next->next;
            int isFunction = 0;
            for(i=0; i<aux->nSymbols; i++)
            {
                comp1 = stringToLower(current->children[0]->var);
                comp2 = stringToLower(aux->symbols[i]->name);
                if(strcmp(comp1, comp2) == 0)
                {
                    free(comp1);
                    free(comp2);
                    if(aux->symbols[i]->type == functionSymbol)
                    {
                        isFunction = 1;
                        break;
                    }
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
            }
            if(!isFunction)
            {
                aux = start;
                for(i=0; i<aux->nSymbols; i++)
                {
                    comp1 = stringToLower(current->children[0]->var);
                    comp2 = stringToLower(aux->symbols[i]->name);
                    if(strcmp(comp1, comp2) == 0)
                    {
                        free(comp1);
                        free(comp2);
                        if(aux->symbols[i]->type == functionSymbol)
                        {
                            isFunction = 1;
                            break;
                        }
                    }
                    else
                    {
                        free(comp1);
                        free(comp2);
                    }
                }
            }
            if(!isFunction)
            {
                newError = (char*)calloc(1024, sizeof(char));
                sprintf(newError, "Function identifier expected");
                addError(newError, current->children[0]->line, current->children[0]->column);
                return 1;
            }
            aux=start->next;
            while(aux!=0)
            {
                if(aux->type==programTable)
                {
                    aux = aux->next;
                    continue;
                }
                comp1 = stringToLower(aux->symbols[0]->name);
                comp2 = stringToLower(current->children[0]->var);
                if(strcmp(comp1, comp2) == 0)
                {
                    free(comp1);
                    free(comp2);
                    int nParams = 0;
                    for(i=1; i<aux->nSymbols; i++)
                    {
                        if(aux->symbols[i]->flag == paramFlag || aux->symbols[i]->flag == varParamFlag)
                        {
                            nParams ++;
                        }
                    }
                    if(nParams != current->nChildren-1)
                    {
                        newError = (char*)calloc(1024, sizeof(char));
                        sprintf(newError, "Wrong number of arguments in call to function %s (got %d, expected %d)",current->children[0]->var, current->nChildren-1, nParams);
                        addError(newError, current->children[0]->line, current->children[0]->column);
                        return 1;
                    }
                    for(i=1; i<nParams+1; i++)
                    {
                        if(aux->symbols[i]->flag == varParamFlag)
                        {
                            if(current->children[i]->nodeType != idNode)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Variable identifier expected");
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                            if(current->children[i]->varType == idVar || current->children[i]->varType == unknownVar || current->children[i]->varType == opVar)
                            {
                                semanticCheck(current->children[i], scope);
                            }
                            if(aux->symbols[i]->type == integerSymbol && current->children[i]->varType != intVar)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _integer_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                            else if(aux->symbols[i]->type == realSymbol && (current->children[i]->varType != realVar && current->children[i]->varType != intVar))
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _real_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                            if(aux->symbols[i]->type == booleanSymbol && current->children[i]->varType != booleanVar)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _boolean_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                        }
                        else if(aux->symbols[i]->flag == paramFlag)
                        {
                            if(current->children[i]->varType == idVar || current->children[i]->varType == unknownVar || current->children[i]->varType == opVar)
                            {
                                semanticCheck(current->children[i], scope);
                            }
                            if(aux->symbols[i]->type == integerSymbol && current->children[i]->varType != intVar)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _integer_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                            else if(aux->symbols[i]->type == realSymbol && (current->children[i]->varType != realVar && current->children[i]->varType != intVar))
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _real_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                            if(aux->symbols[i]->type == booleanSymbol && current->children[i]->varType != booleanVar)
                            {
                                newError = (char*)calloc(1024, sizeof(char));
                                sprintf(newError, "Incompatible type for argument %d in call to function %s (got %s, expected _boolean_)", i, current->children[0]->var, printableTypes[current->children[i]->varType]);
                                addError(newError, current->children[i]->line, current->children[i]->column);
                                return 1;
                            }
                        }
                    }
                    switch(aux->symbols[0]->type)
                    {
                        case integerSymbol:
                            current->varType = intVar;
                            return 0;
                        case realSymbol:
                            current->varType = realVar;
                            return 0;
                        case booleanSymbol:
                            current->varType = booleanVar;
                            return 0;
                        default:
                            current->varType = unknownVar;
                            return 0;
                    }
                }
                else
                {
                    free(comp1);
                    free(comp2);
                }
                aux = aux->next;
            }
            break;
        case writeLnNode:
            if(current->nChildren == 0)
            {
                return 0;
            }
            for(i=0; i<current->nChildren; i++)
            {
                if(current->children[i]->varType == idVar || current->children[i]->varType == unknownVar || current->children[i]->varType == opVar)
                {
                    semanticCheck(current->children[i], scope);
                }
                if(current->children[i]->varType == typeVar || current->children[i]->varType == unknownVar)
                {
                    newError = (char*)calloc(1024, sizeof(char));
                    sprintf(newError, "Cannot write values of type %s", printableTypes[current->children[i]->varType]);
                    addError(newError, current->children[i]->line, current->children[i]->column);
                    return 1;
                }
            }
        default:
            for(i=0; i<current->nChildren; i++)
            {
                sum+=semanticCheck(current->children[i], scope);
            }
            return sum;
    }
    return 0;
}
