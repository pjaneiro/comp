#ifndef SYMBOLS_H_INCLUDED
#define SYMBOLS_H_INCLUDED

typedef enum {outerTable, programTable, functionTable} table_type_t;
typedef enum {booleanSymbol, integerSymbol, realSymbol, functionSymbol, programSymbol, typeSymbol, noSymbol} symbol_type_t;
typedef enum {constantFlag, returnFlag, paramFlag, varParamFlag, noFlag} flag_type_t;
typedef struct symbol
{
    char* name;
    symbol_type_t type;
    flag_type_t flag;
    char* value;
    int line;
    int column;
} symbol;
typedef struct table
{
    table_type_t type;
    struct table* next;
    struct table* previous;
    int nSymbols;
    symbol** symbols;
} table;

symbol* createSymbol(char*, symbol_type_t, flag_type_t, char*, int, int);
table* createTable(table_type_t);
void addSymbolToTable(table*, symbol*);
table* createAllTables();
void clearTables(table*);

void printTables(table*);
void printSymbol(symbol*);
char* stringToLower(char*);
symbol_type_t symbolFromString(char*);
table* start;

void addError(char*, int, int);
int semanticCheck(node*, char*);

#endif // SYMBOLS_H_INCLUDED