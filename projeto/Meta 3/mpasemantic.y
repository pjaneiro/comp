%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"
#include "symbols.h"

void yyerror(char* s);

extern int yylineno;
extern int column;
extern char* yytext;
extern int yyleng;

int yylex();

extern int semLine;
extern int semColumn;
extern char* semError;

node* aux;
node* foo;

int printFlag = 0;
int tableFlag = 0;
%}

%union
{
    struct node* no;
    struct data val;
}

%token <val> ASSIGNSYM BEGINSYM COLONSYM COMMASYM DOSYM DOTSYM ELSESYM ENDSYM FORWARDSYM FUNCTIONSYM IFSYM LBRACSYM NOTSYM OUTPUTSYM PARAMSTRSYM PROGRAMSYM RBRACSYM REPEATSYM SEMICSYM THENSYM UNTILSYM VALSYM VARSYM WHILESYM WRITELNSYM;
%token <val> RESERVED ID STRING INTLIT REALLIT
%token <val> LT GT EQ NEQ LEQ GEQ PLUS MINUS OR MUL REALDIV MOD DIV AND

%nonassoc ID INTLIT REALLIT STRING
%nonassoc BEGINSYM COLONSYM COMMASYM DOTSYM ENDSYM FORWARDSYM FUNCTIONSYM OUTPUTSYM PARAMSTRSYM PROGRAMSYM REPEATSYM SEMICSYM UNTILSYM VALSYM VARSYM WRITELNSYM
%nonassoc RESERVED
%nonassoc THENSYM DOSYM
%nonassoc ELSESYM
%nonassoc IFSYM WHILESYM
%left LT GT EQ NEQ LEQ GEQ
%left PLUS MINUS OR
%left MUL REALDIV MOD DIV AND
%right NOTSYM
%left LBRACSYM RBRACSYM
%right ASSIGNSYM

%type <no> prog progBlock progHeading varPart optVarPart varDeclaration IDList optIDList funcPart funcDeclaration funcHeading funcIdent formalParamList optFormalParamsList formalParams funcBlock statPart compStat statList optStatList stat writelnPList optWritelnPList expr paramList optParamList simpleExpr term factor functionDesignator

%%

prog: progHeading SEMICSYM progBlock DOTSYM                                         {
                                                                                        $$ = createNode(programNode, "Program", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                        root=$$;
                                                                                        free($2.val);
                                                                                        free($4.val);
                                                                                    }
;

progBlock: varPart funcPart statPart                                                {
                                                                                        $$ = createNode(varPartNode, "VarPart", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        aux = createNode(funcPartNode, "FuncPart", opVar, OP);
                                                                                        addChild(aux, $2);
                                                                                        addSibling($$, aux);
                                                                                        if($3 != NULL)
                                                                                        {
                                                                                            if($3->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $3);
                                                                                                addSibling($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addSibling($$, $3);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addSibling($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                    }

progHeading: PROGRAMSYM ID LBRACSYM OUTPUTSYM RBRACSYM                              {
                                                                                        $$ = createNode(idNode, "ID", idVar, $2);
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                        free($4.val);
                                                                                        free($5.val);
                                                                                    }
;

varPart:                                                                            {
                                                                                        $$ = NULL;
                                                                                    }
       | VARSYM varDeclaration SEMICSYM optVarPart                                  {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $4);
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                    }
;

optVarPart:                                                                         {
                                                                                        $$ = NULL;
                                                                                    }
          | varDeclaration SEMICSYM optVarPart                                      {
                                                                                        $$ = $1;
                                                                                        addSibling($$, $3);
                                                                                        free($2.val);
                                                                                    }
;

varDeclaration: IDList COLONSYM ID                                                  {
                                                                                        $$ = createNode(varDeclNode, "VarDecl", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, createNode(idNode, "ID", typeVar, $3));
                                                                                        free($2.val);
                                                                                    }
;

IDList: ID optIDList                                                                {
                                                                                        $$ = createNode(idNode, "ID", idVar, $1);
                                                                                        addSibling($$, $2);
                                                                                    }
;

optIDList:                                                                          {
                                                                                        $$ = NULL;
                                                                                    }
         | COMMASYM ID optIDList                                                    {
                                                                                        $$ = createNode(idNode, "ID", idVar, $2);
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                    }
;

funcPart:                                                                           {
                                                                                        $$ = NULL;
                                                                                    }
        | funcDeclaration SEMICSYM funcPart                                         {
                                                                                        $$ = $1;
                                                                                        addSibling($$, $3);
                                                                                        free($2.val);
                                                                                    }
;

funcDeclaration: funcHeading SEMICSYM FORWARDSYM                                    {
                                                                                        $$ = createNode(funcDeclNode, "FuncDecl", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        free($2.val);
                                                                                        free($3.val);
                                                                                    }
               | funcIdent SEMICSYM funcBlock                                       {
                                                                                        $$ = createNode(funcDef2Node, "FuncDef2", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                        free($2.val);
                                                                                    }
               | funcHeading SEMICSYM funcBlock                                     {
                                                                                        $$ = createNode(funcDefNode, "FuncDef", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                        free($2.val);
                                                                                    }
;

funcHeading: FUNCTIONSYM ID COLONSYM ID                                             {
                                                                                        $$ = createNode(idNode, "ID", idVar, $2);
                                                                                        addSibling($$, createNode(funcParamsNode, "FuncParams", opVar, OP));
                                                                                        addSibling($$, createNode(idNode, "ID", typeVar, $4));
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                    }
           | FUNCTIONSYM ID formalParamList COLONSYM ID                             {
                                                                                        $$ = createNode(idNode, "ID", idVar, $2);
                                                                                        addSibling($$, $3);
                                                                                        addSibling($$, createNode(idNode, "ID", typeVar, $5));
                                                                                        free($1.val);
                                                                                        free($4.val);
                                                                                    }
;

funcIdent: FUNCTIONSYM ID                                                           {
                                                                                        $$ = createNode(idNode, "ID", idVar, $2);
                                                                                        free($1.val);
                                                                                    }
;

formalParamList: LBRACSYM formalParams optFormalParamsList RBRACSYM                 {
                                                                                        $$ = createNode(funcParamsNode, "FuncParams", opVar, OP);
                                                                                        addChild($$, $2);
                                                                                        addChild($$, $3);
                                                                                        free($1.val);
                                                                                        free($4.val);
                                                                                    }
;

optFormalParamsList:                                                                {
                                                                                        $$ = NULL;
                                                                                    }
                   | SEMICSYM formalParams optFormalParamsList                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$,$3);
                                                                                        free($1.val);
                                                                                    }
;

formalParams: IDList COLONSYM ID                                                    {
                                                                                        $$ = createNode(paramsNode, "Params", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        aux = createNode(idNode, "ID", typeVar, $3);
                                                                                        addChild($$,aux);
                                                                                        free($2.val);
                                                                                    }
            | VARSYM IDList COLONSYM ID                                             {
                                                                                        $$ = createNode(varParamsNode, "VarParams", opVar, OP);
                                                                                        addChild($$, $2);
                                                                                        aux = createNode(idNode, "ID", typeVar, $4);
                                                                                        addChild($$,aux);
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                    }
;

funcBlock: varPart statPart                                                         {
                                                                                        $$ = createNode(varPartNode, "VarPart", opVar, OP);
                                                                                        addChild($$, $1);
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $2);
                                                                                                addSibling($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addSibling($$, $2);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addSibling($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                    }
;

statPart: compStat                                                                  {
                                                                                        if($1 != NULL)
                                                                                        {
                                                                                            if($1->right != NULL)
                                                                                            {
                                                                                                $$ = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild($$, $1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $$ = $1;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = NULL;
                                                                                        }
                                                                                    }
;

compStat: BEGINSYM statList ENDSYM                                                  {
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                $$ = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild($$, $2);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $$ = $2;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = NULL;
                                                                                        }
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                    }
;

statList: stat optStatList                                                          {
                                                                                        if($1 != NULL)
                                                                                        {
                                                                                            $$ = $1;
                                                                                            addSibling($$, $2);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = $2;
                                                                                        }
                                                                                    }
;

optStatList:                                                                        {
                                                                                        $$ = NULL;
                                                                                    }
           | SEMICSYM stat optStatList                                              {
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            $$ = $2;
                                                                                            addSibling($$, $3);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = $3;
                                                                                        }
                                                                                        free($1.val);
                                                                                    }
;

stat: compStat                                                                      {
                                                                                        $$ = $1;
                                                                                    }
    | IFSYM expr THENSYM stat                                                       {
                                                                                        $$ = createNode(ifElseNode, "IfElse", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                        aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                        addChild($$,aux);
                                                                                        free($3.val);
                                                                                    }
    | IFSYM expr THENSYM stat ELSESYM stat                                          {
                                                                                        $$ = createNode(ifElseNode, "IfElse", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                        if($6 != NULL)
                                                                                        {
                                                                                            if($6->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $6);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $6);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(statListNode, "StatList", opVar, $5));
                                                                                        }
                                                                                        free($3.val);
                                                                                        free($5.val);
                                                                                    }
    | WHILESYM expr DOSYM stat                                                      {
                                                                                        $$ = createNode(whileNode, "While", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                        free($3.val);
                                                                                    }
    | REPEATSYM statList UNTILSYM expr                                              {
                                                                                        $$ = createNode(repeatNode, "Repeat", opVar, $1);
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                aux = createNode(statListNode, "StatList", opVar, OP);
                                                                                                addChild(aux, $2);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $2);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(statListNode, "StatList", opVar, OP));
                                                                                        }
                                                                                        addChild($$, $4);
                                                                                        free($3.val);
                                                                                    }
    | VALSYM LBRACSYM PARAMSTRSYM LBRACSYM expr RBRACSYM COMMASYM ID RBRACSYM       {
                                                                                        $$ = createNode(valParamNode, "ValParam", opVar, $3);
                                                                                        addChild($$, $5);
                                                                                        addChild($$, createNode(idNode, "ID", idVar, $8));
                                                                                        free($1.val);
                                                                                        free($2.val);
                                                                                        free($4.val);
                                                                                        free($6.val);
                                                                                        free($7.val);
                                                                                        free($9.val);
                                                                                    }
    | ID ASSIGNSYM expr                                                             {
                                                                                        $$ = createNode(assignNode, "Assign", opVar, $2);
                                                                                        addChild($$, createNode(idNode, "ID", idVar, $1));
                                                                                        addChild($$, $3);
                                                                                    }
    |                                                                               {
                                                                                        $$ = NULL;
                                                                                    }
    | WRITELNSYM writelnPList                                                       {
                                                                                        $$ = createNode(writeLnNode, "WriteLn", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                    }
    | WRITELNSYM                                                                    {
                                                                                        $$ = createNode(writeLnNode, "WriteLn", opVar, $1);
                                                                                    }
;

writelnPList: LBRACSYM expr optWritelnPList RBRACSYM                                {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                        free($4.val);
                                                                                    }
            | LBRACSYM STRING optWritelnPList RBRACSYM                              {
                                                                                        $$ = createNode(stringNode, "String", stringVar, $2);
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                        free($4.val);
                                                                                    }
;

optWritelnPList:                                                                    {
                                                                                        $$ = NULL;
                                                                                    }
               | COMMASYM expr optWritelnPList                                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                    }
               | COMMASYM STRING optWritelnPList                                    {
                                                                                        $$ = createNode(stringNode, "String", stringVar, $2);
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                    }
;

expr: simpleExpr                                                                    {
                                                                                        $$ = $1;
                                                                                    }
    | simpleExpr LT simpleExpr                                                      {
                                                                                        $$ = createNode(ltNode, "Lt", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr GT simpleExpr                                                      {
                                                                                        $$ = createNode(gtNode, "Gt", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }

    | simpleExpr EQ simpleExpr                                                      {
                                                                                        $$ = createNode(eqNode, "Eq", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr NEQ simpleExpr                                                     {
                                                                                        $$ = createNode(neqNode, "Neq", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr LEQ simpleExpr                                                     {
                                                                                        $$ = createNode(leqNode, "Leq", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr GEQ simpleExpr                                                     {
                                                                                        $$ = createNode(geqNode, "Geq", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
;

simpleExpr: simpleExpr PLUS term                                                    {
                                                                                        $$ = createNode(addNode, "Add", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | simpleExpr MINUS term                                                   {
                                                                                        $$ = createNode(subNode, "Sub", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | simpleExpr OR term                                                      {
                                                                                        $$ = createNode(orNode, "Or", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | PLUS term                                                               {
                                                                                        $$ = createNode(plusNode, "Plus", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                    }
          | MINUS term                                                              {
                                                                                        $$ = createNode(minusNode, "Minus", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                    }
          | term                                                                    {
                                                                                        $$ = $1;
                                                                                    }
;

term: term MUL term                                                                 {
                                                                                        $$ = createNode(mulNode, "Mul", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term REALDIV term                                                             {
                                                                                        $$ = createNode(realDivNode, "RealDiv", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term DIV term                                                                 {
                                                                                        $$ = createNode(divNode, "Div", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term MOD term                                                                 {
                                                                                        $$ = createNode(modNode, "Mod", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term AND term                                                                 {
                                                                                        $$ = createNode(andNode, "And", opVar, $2);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | factor                                                                        {
                                                                                        $$ = $1;
                                                                                    }
;

factor: ID                                                                          {
                                                                                        $$ = createNode(idNode, "ID", idVar, $1);
                                                                                    }
      | INTLIT                                                                      {
                                                                                        $$ = createNode(intLitNode, "IntLit", intVar, $1);
                                                                                    }
      | REALLIT                                                                     {
                                                                                        $$ = createNode(realLitNode, "RealLit", realVar, $1);
                                                                                    }
      | functionDesignator                                                          {
                                                                                        $$ = $1;
                                                                                    }
      | LBRACSYM expr RBRACSYM                                                      {
                                                                                        $$ = $2;
                                                                                        free($1.val);
                                                                                        free($3.val);
                                                                                    }
      | NOTSYM factor                                                               {
                                                                                        $$ = createNode(notNode, "Not", opVar, $1);
                                                                                        addChild($$, $2);
                                                                                    }
;

functionDesignator: ID paramList                                                    {
                                                                                        $$ = createNode(callNode, "Call", opVar, OP);
                                                                                        addChild($$, createNode(idNode, "ID", idVar, $1));
                                                                                        addChild($$, $2);
                                                                                    }
;

paramList: LBRACSYM expr optParamList RBRACSYM                                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                        free($4.val);
                                                                                    }
;

optParamList:                                                                       {
                                                                                        $$ = NULL;
                                                                                    }
            | COMMASYM expr optParamList                                            {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                        free($1.val);
                                                                                    }
;

%%
int main(int argc, char* argv[])
{
    root = 0;
    start = 0;
    if(argc!=1)
    {
        int i;
        for(i=1; i<argc; i++)
        {
            if(strcmp(argv[i],"-t")==0)
            {
                printFlag = 1;
            }
            if(strcmp(argv[i],"-s")==0)
            {
                tableFlag = 1;
            }
        }
    }
    yyparse();
    if(printFlag)
    {
        printTree(root,0);
        if(tableFlag)
        {
            printf("\n");
        }
    }
    start = createAllTables();
    semanticCheck(root, "");
    if(semError==0)
    {
        if(tableFlag)
        {
            printTables(start);
        }
    }
    else if(tableFlag)
    {
        printf("Line %d, col %d: %s\n", semLine, semColumn, semError);
    }
    clearTree(root);
    clearTables(start);
    free(semError);
    return 0;
}

void yyerror(char* s)
{
    printf("Line %d, col %lu: %s: %s\n", yylineno, (unsigned long)(column-strlen(yytext)), s, yytext);
    clearTree(root);
    exit(0);
}
