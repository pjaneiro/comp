#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

typedef enum {id_node, intLit_node, realLit_node, string_node, op_node, unknown_node} node_type_t;
typedef struct node
{
    node_type_t nodeType;
    char* nodeTypeString;
    char* var;
    struct node** children;
    int nChildren;
    struct node* parent;
    struct node* left;
    struct node* right;
} node;

node* createNode(node_type_t, char*, char*);
void addChild(node*, node*);
void addSibling(node*, node*);
void printTree(node*, int);
void clearTree(node*);

#define OP (char*)calloc(1,sizeof(char))

#endif // TREE_H_INCLUDED