%option yylineno
%option case-insensitive
%option noinput
%option nounput

%x COMMENT

%{
#include "tree.h"
#include "symbols.h"
#include "y.tab.h"
#include <ctype.h>
int commentL, column = 1, i, commentC;
#define YY_USER_ACTION yylval.val.val = (char*)calloc(1+yyleng,sizeof(char)); strncpy(yylval.val.val,yytext,yyleng); yylval.val.line=yylineno; yylval.val.column=column; column+=yyleng; 
%}

RESERVED                                        abs|arctan|array|case|char|chr|const|cos|dispose|downto|eof|eoln|exp|file|for|get|goto|in|input|label|ln|maxint|new|nil|odd|of|ord|pack|packed|page|pred|procedure|put|read|readln|record|reset|rewrite|round|set|sin|sqr|sqrt|succ|text|to|trunc|type|unpack|with|write

%%
%{
//Comments(complete/incomplete) first, since they're ignored, followed by the token separators, ' ', '\t' and '\n'
%}
\{|\(\*                                         {BEGIN COMMENT; commentL=yylineno; commentC=(int)(column-yyleng);}
<COMMENT>\}|\*\)                                {BEGIN 0;}
<COMMENT>.                                      {;}
<COMMENT>\n                                     {column=1;}
<COMMENT><<EOF>>                                {printf("Line %d, col %d: unterminated comment\n", commentL, commentC); BEGIN 0;}
\n                                              {column=1;}
\ |\t                                           {;}
%{
//Main operations and symbols, by the order they're given
%}
\:\=                                            {return ASSIGNSYM;}
begin                                           {return BEGINSYM;}
\:                                              {return COLONSYM;}
\,                                              {return COMMASYM;}
do                                              {return DOSYM;}
\.                                              {return DOTSYM;}
else                                            {return ELSESYM;}
end                                             {return ENDSYM;}
forward                                         {return FORWARDSYM;}
function                                        {return FUNCTIONSYM;}
if                                              {return IFSYM;}
\(                                              {return LBRACSYM;}
not                                             {return NOTSYM;}
output                                          {return OUTPUTSYM;}
paramstr                                        {return PARAMSTRSYM;}
program                                         {return PROGRAMSYM;}
\)                                              {return RBRACSYM;}
repeat                                          {return REPEATSYM;}
\;                                              {return SEMICSYM;}
then                                            {return THENSYM;}
until                                           {return UNTILSYM;}
val                                             {return VALSYM;}
var                                             {return VARSYM;}
while                                           {return WHILESYM;}
writeln                                         {return WRITELNSYM;}
%{
//Operators
%}
and                                             {return AND;}
or                                              {return OR;}
\<												{return LT;}
\>												{return GT;}
\=												{return EQ;}
\<\>											{return NEQ;}
\<\=											{return LEQ;}
\>\=											{return GEQ;}
\+												{return PLUS;}
\-												{return MINUS;}
\*												{return MUL;}
\/												{return REALDIV;}
mod												{return MOD;}
div												{return DIV;}
%{
//Reserved keywords
%}
{RESERVED}                                      {return RESERVED;}
%{
//IDs and numbers
%}
[a-z]+[a-z0-9]*                                 {return ID;}
[0-9]+                                          {return INTLIT;}
[0-9]+\.[0-9]+                                  {return REALLIT;}
[0-9]+(\.[0-9]+)?e(\+|\-)?[0-9]+                {return REALLIT;}
%{
//Strings (complete/incomplete)
%}
'(''|[^'\n])*'                                  {return STRING;}
'(''|[^'\n])*                                   {printf("Line %d, col %d: unterminated string\n", yylineno, (int)(column-yyleng));}
%{
//All else are errors
%}
.                                               {printf("Line %d, col %d: illegal character ('%s')\n", yylineno, (int)(column-yyleng), yytext);}
%%

int yywrap()
{
    return 1;
}

