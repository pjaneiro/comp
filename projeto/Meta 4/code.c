#include <stdlib.h>
#include <stdio.h>
#include "tree.h"
#include "symbols.h"
#include "code.h"

void printHeader()
{
    printf("declare i32 @printf(i8*, ...)\n");
    printf("declare i32 @atoi(i8*) nounwind readonly\n");
    printf("\n\n");

    printf("@str.int_lit = private unnamed_addr constant [3 x i8] c\"%%d\\00\"\n");
    printf("@str.real_lit = private unnamed_addr constant [6 x i8] c\"%%.12E\\00\"\n");
    printf("@str.str = private unnamed_addr constant [3 x i8] c\"%%s\\00\", align 1\n");
    printf("@str.false = private unnamed_addr constant [6 x i8] c\"FALSE\\00\"\n");
    printf("@str.true = private unnamed_addr constant [5 x i8] c\"TRUE\\00\"\n");
    printf("@str.blank = private unnamed_addr constant [2 x i8] c\"\\0A\\00\", align 1\n");
    printf("@str.bools = global [2 x i8*] [i8* getelementptr inbounds ([6 x i8]* @str.false, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @str.true, i32 0, i32 0)]\n");
    printf("\n\n");
}

void printLLVM()
{
	printHeader();
	createLLVM(root);
}

void createLLVM(node* current)
{
	static int variableCounter = 1;
	int i;
	switch(current->nodeType)
	{
		case writeLnNode:
		{
        	for(i=0; i<current->nChildren; i++)
        	{
        		switch(current->children[i]->varType)
        		{
        			case intVar:
        			{
        				printf("\t%%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @str.int_lit, i32 0, i32 0), i32 %d)\n", variableCounter++, atoi(current->children[i]->var));
        				break;
        			}
        			case realVar:
        			{
        				printf("\t%%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([6 x i8]* @str.real_lit, i32 0, i32 0), double %f)\n", variableCounter++, atof(current->children[i]->var));
        				break;
        			}
        			case stringVar:
        			{
        				printf("\t%%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @str.str, i32 0, i32 0), i8* %s)\n", variableCounter++, current->children[i]->var);
        				break;
        			}
        			default:
        			{
        				break;
        			}
        		}
        	}
        	printf("\t%%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([2 x i8]* @str.blank, i32 0, i32 0))\n", variableCounter++);
            break;
		}
		case programNode:
		{
			printf("define i32 @main(i32 %%argc, i8** %%argv) #0 {\n");
			printf("\t%%%d = alloca i32, align 4\n", variableCounter++);
			printf("\t%%%d = alloca i32, align 4\n", variableCounter++);
			printf("\t%%%d = alloca i8**, align 8\n", variableCounter++);
			printf("\tstore i32 0, i32* %%1\n");
			printf("\tstore i32 %%argc, i32* %%2, align 4\n");
			printf("\tstore i8** %%argv, i8*** %%3, align 8\n");
			createLLVM(current->children[3]);
			printf("\tret i32 0\n");
			printf("}\n");
			break;
		}
		default:
		{
			for(i=0; i<current->nChildren; i++)
			{
				createLLVM(current->children[i]);
			}
		}
	}
}
