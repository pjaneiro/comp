\select@language {english}
\contentsline {chapter}{\numberline {1}Siglas e Abrevia\IeC {\c c}\IeC {\~o}es}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Palavras-Chave}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Introdu\IeC {\c c}\IeC {\~a}o}{4}{chapter.3}
\contentsline {section}{\numberline {3.1}Linguagem mili-Pascal}{4}{section.3.1}
\contentsline {section}{\numberline {3.2}Programa em mili-Pascal}{4}{section.3.2}
\contentsline {chapter}{\numberline {4}An\IeC {\'a}lise lexical}{5}{chapter.4}
\contentsline {section}{\numberline {4.1}Tokens}{5}{section.4.1}
\contentsline {section}{\numberline {4.2}Tratamento de erros lexicais}{8}{section.4.2}
\contentsline {chapter}{\numberline {5}An\IeC {\'a}lise sint\IeC {\'a}tica}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Gram\IeC {\'a}tica mili-Pascal (nota\IeC {\c c}\IeC {\~a}o EBNF)}{9}{section.5.1}
\contentsline {section}{\numberline {5.2}Gram\IeC {\'a}tica adaptada}{11}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Preced\IeC {\^e}ncias}{11}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Regras}{11}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}\IeC {\'A}rvore de Sintaxe Abstrata}{14}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}N\IeC {\'o}s necess\IeC {\'a}rios}{14}{subsection.5.3.1}
\contentsline {subsubsection}{Programa}{14}{section*.2}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de vari\IeC {\'a}veis}{14}{section*.3}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de fun\IeC {\c c}\IeC {\~o}es}{14}{section*.4}
\contentsline {subsubsection}{Statements}{14}{section*.5}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{14}{section*.6}
\contentsline {subsubsection}{Terminais}{14}{section*.7}
\contentsline {subsection}{\numberline {5.3.2}Implementa\IeC {\c c}\IeC {\~a}o}{15}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Exemplo de output}{16}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Tratamento de erros sint\IeC {\'a}ticos}{18}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}An\IeC {\'a}lise sem\IeC {\^a}ntica}{19}{chapter.6}
\contentsline {section}{\numberline {6.1}Tabela de s\IeC {\'\i }mbolos}{19}{section.6.1}
\contentsline {section}{\numberline {6.2}Exemplo de output}{20}{section.6.2}
\contentsline {section}{\numberline {6.3}Tratamento de erros sem\IeC {\^a}nticos}{21}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Erros poss\IeC {\'\i }veis}{21}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Implementa\IeC {\c c}\IeC {\~a}o}{22}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Output completo (AST + tabela de s\IeC {\'\i }mbolos)}{23}{section.6.4}
\contentsline {chapter}{\numberline {7}Gera\IeC {\c c}\IeC {\~a}o de c\IeC {\'o}digo}{26}{chapter.7}
\contentsline {section}{\numberline {7.1}C\IeC {\'o}digo gen\IeC {\'e}rico}{26}{section.7.1}
\contentsline {section}{\numberline {7.2}Fun\IeC {\c c}\IeC {\~a}o main}{27}{section.7.2}
\contentsline {section}{\numberline {7.3}Writeln}{28}{section.7.3}
\contentsline {chapter}{\numberline {8}Considera\IeC {\c c}\IeC {\~o}es finais}{29}{chapter.8}
\contentsline {section}{\numberline {8.1}Meta 3}{29}{section.8.1}
\contentsline {section}{\numberline {8.2}Meta 4}{29}{section.8.2}
