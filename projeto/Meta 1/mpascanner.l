%option yylineno
%option case-insensitive

%x COMMENT

%{
	#include <ctype.h>
	int commentL;
	unsigned long column = 1, i, commentC;
	#define YY_USER_ACTION column+=yyleng;
%}

RESERVED										abs|arctan|array|case|char|chr|const|cos|dispose|downto|eof|eoln|exp|file|for|get|goto|in|input|label|ln|maxint|new|nil|odd|of|ord|pack|packed|page|pred|procedure|put|read|readln|record|reset|rewrite|round|set|sin|sqr|sqrt|succ|text|to|trunc|type|unpack|with|write

%%
%{
//Comments(complete/incomplete) first, since they're ignored, followed by the token separators, ' ', '\t' and '\n'
%}
\{|\(\*											{BEGIN COMMENT; commentL=yylineno; commentC=(unsigned long)(column-yyleng);}
<COMMENT>\}|\*\)								{BEGIN 0;}
<COMMENT>.										{;}
<COMMENT>\n										{column=1;}
<COMMENT><<EOF>>								{printf("Line %d, col %lu: unterminated comment\n", commentL, commentC); BEGIN 0;}
\n												{column=1;}
\ |\t											{;}
%{
//Main operations and symbols, by the order they're given
%}
\:\=											{printf("ASSIGN\n");}
begin											{printf("BEGIN\n");}
\:												{printf("COLON\n");}
\,												{printf("COMMA\n");}
do												{printf("DO\n");}
\.												{printf("DOT\n");}
else											{printf("ELSE\n");}
end												{printf("END\n");}
forward											{printf("FORWARD\n");}
function										{printf("FUNCTION\n");}
if												{printf("IF\n");}
\(												{printf("LBRAC\n");}
not												{printf("NOT\n");}
output											{printf("OUTPUT\n");}
paramstr										{printf("PARAMSTR\n");}
program											{printf("PROGRAM\n");}
\)												{printf("RBRAC\n");}
repeat											{printf("REPEAT\n");}
\;												{printf("SEMIC\n");}
then											{printf("THEN\n");}
until											{printf("UNTIL\n");}
val												{printf("VAL\n");}
var												{printf("VAR\n");}
while											{printf("WHILE\n");}
writeln											{printf("WRITELN\n");}
%{
//Operators
%}
and|or											{printf("OP1(%s)\n",yytext);}
\<|\>|\=|\<\>|\<\=|\>\=							{printf("OP2(%s)\n",yytext);}
\+|\-											{printf("OP3(%s)\n",yytext);}
\*|\/|mod|div									{printf("OP4(%s)\n",yytext);}
%{
//Reserved keywords
%}
{RESERVED}										{printf("RESERVED(%s)\n", yytext);}
%{
//IDs and numbers
%}
[a-z]+[a-z0-9]*									{printf("ID(%s)\n",yytext);}
[0-9]+											{printf("INTLIT(%s)\n",yytext);}
[0-9]+\.[0-9]+									{printf("REALLIT(%s)\n",yytext);}
[0-9]+(\.[0-9]+)?e(\+|\-)?[0-9]+				{printf("REALLIT(%s)\n",yytext);}
%{
//Strings (complete/incomplete)
%}
'(''|[^'\n])*'									{printf("STRING(%s)\n",yytext);}
'(''|[^'\n])*									{printf("Line %d, col %lu: unterminated string\n", yylineno, (unsigned long)(column-yyleng));}
%{
//All else are errors
%}
.												{printf("Line %d, col %lu: illegal character ('%s')\n", yylineno, (unsigned long)(column-yyleng), yytext);}
%%
int main()
{
	yylex();
	return 0;
}

int yywrap()
{
	return 1;
}
