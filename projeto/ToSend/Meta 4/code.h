#ifndef CODE_H_INCLUDED
#define CODE_H_INCLUDED

void printHeader();
void printLLVM();
void createLLVM(node*);

#endif // CODE_H_INCLUDED