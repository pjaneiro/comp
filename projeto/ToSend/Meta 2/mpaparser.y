%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"
#define NSYMS 100

void yyerror(char* s);

extern int yylineno;
extern unsigned long int column;
extern char* yytext;
extern int yyleng;

node* root;
node* aux;
node* foo;

int printFlag = 0;
%}

%union
{
    char* val;
    struct node* no;
}

%token ASSIGNSYM BEGINSYM COLONSYM COMMASYM DOSYM DOTSYM ELSESYM ENDSYM FORWARDSYM FUNCTIONSYM IFSYM LBRACSYM NOTSYM OUTPUTSYM PARAMSTRSYM PROGRAMSYM RBRACSYM REPEATSYM SEMICSYM THENSYM UNTILSYM VALSYM VARSYM WHILESYM WRITELNSYM;
%token <val> RESERVED ID STRING INTLIT REALLIT

%nonassoc ID INTLIT REALLIT STRING
%nonassoc BEGINSYM COLONSYM COMMASYM DOTSYM ENDSYM FORWARDSYM FUNCTIONSYM OUTPUTSYM PARAMSTRSYM PROGRAMSYM REPEATSYM SEMICSYM UNTILSYM VALSYM VARSYM WRITELNSYM
%nonassoc RESERVED
%nonassoc THENSYM DOSYM
%nonassoc ELSESYM
%nonassoc IFSYM WHILESYM
%left LT GT EQ NEQ LEQ GEQ
%left PLUS MINUS OR
%left MUL REALDIV MOD DIV AND
%right NOTSYM
%left LBRACSYM RBRACSYM
%right ASSIGNSYM

%type <no> prog progBlock progHeading varPart optVarPart varDeclaration IDList optIDList funcPart funcDeclaration funcHeading funcIdent formalParamList optFormalParamsList formalParams funcBlock statPart compStat statList optStatList stat writelnPList optWritelnPList expr paramList optParamList simpleExpr term factor functionDesignator

%%

prog: progHeading SEMICSYM progBlock DOTSYM                                         {
                                                                                        $$ = createNode(op_node, "Program", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                        root=$$;
                                                                                    }
;

progBlock: varPart funcPart statPart                                                {
                                                                                        $$ = createNode(op_node, "VarPart", OP);
                                                                                        addChild($$, $1);
                                                                                        aux = createNode(op_node, "FuncPart", OP);
                                                                                        addChild(aux, $2);
                                                                                        addSibling($$, aux);
                                                                                        if($3 != NULL)
                                                                                        {
                                                                                            if($3->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $3);
                                                                                                addSibling($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addSibling($$, $3);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addSibling($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                    }

progHeading: PROGRAMSYM ID LBRACSYM OUTPUTSYM RBRACSYM                              {
                                                                                        $$ = createNode(id_node, "ProgHeading", $2);
                                                                                    }
;

varPart:                                                                            {
                                                                                        $$ = NULL;
                                                                                    }
       | VARSYM varDeclaration SEMICSYM optVarPart                                  {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $4);
                                                                                    }
;

optVarPart:                                                                         {
                                                                                        $$ = NULL;
                                                                                    }
          | varDeclaration SEMICSYM optVarPart                                      {
                                                                                        $$ = $1;
                                                                                        addSibling($$, $3);
                                                                                    }
;

varDeclaration: IDList COLONSYM ID                                                  {
                                                                                        $$ = createNode(op_node, "VarDecl", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, createNode(id_node, "ID", $3));
                                                                                    }
;

IDList: ID optIDList                                                                {
                                                                                        $$ = createNode(id_node, "ID", $1);
                                                                                        addSibling($$, $2);
                                                                                    }
;

optIDList:                                                                          {
                                                                                        $$ = NULL;
                                                                                    }
         | COMMASYM ID optIDList                                                    {
                                                                                        $$ = createNode(id_node, "ID", $2);
                                                                                        addSibling($$, $3);
                                                                                    }
;

funcPart:                                                                           {
                                                                                        $$ = NULL;
                                                                                    }
        | funcDeclaration SEMICSYM funcPart                                         {
                                                                                        $$ = $1;
                                                                                        addSibling($$, $3);
                                                                                    }
;

funcDeclaration: funcHeading SEMICSYM FORWARDSYM                                    {
                                                                                        $$ = createNode(op_node, "FuncDecl", OP);
                                                                                        addChild($$, $1);
                                                                                    }
               | funcIdent SEMICSYM funcBlock                                       {
                                                                                        $$ = createNode(op_node, "FuncDef2", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
               | funcHeading SEMICSYM funcBlock                                     {
                                                                                        $$ = createNode(op_node, "FuncDef", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
;

funcHeading: FUNCTIONSYM ID COLONSYM ID                                             {
                                                                                        $$ = createNode(id_node, "ID", $2);
                                                                                        addSibling($$, createNode(op_node, "FuncParams", OP));
                                                                                        addSibling($$, createNode(id_node, "ID", $4));
                                                                                    }
           | FUNCTIONSYM ID formalParamList COLONSYM ID                             {
                                                                                        $$ = createNode(id_node, "ID", $2);
                                                                                        addSibling($$, $3);
                                                                                        addSibling($$, createNode(id_node, "ID", $5));
                                                                                    }
;

funcIdent: FUNCTIONSYM ID                                                           {
                                                                                        $$ = createNode(id_node, "funcIdent", $2);
                                                                                    }
;

formalParamList: LBRACSYM formalParams optFormalParamsList RBRACSYM                 {
                                                                                        $$ = createNode(op_node, "FuncParams", OP);
                                                                                        addChild($$, $2);
                                                                                        addChild($$, $3);
                                                                                    }
;

optFormalParamsList:                                                                {
                                                                                        $$ = NULL;
                                                                                    }
                   | SEMICSYM formalParams optFormalParamsList                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$,$3);
                                                                                    }
;

formalParams: IDList COLONSYM ID                                                    {
                                                                                        $$ = createNode(op_node, "Params", OP);
                                                                                        addChild($$, $1);
                                                                                        aux = createNode(id_node, "ID", $3);
                                                                                        addChild($$,aux);
                                                                                    }
            | VARSYM IDList COLONSYM ID                                             {
                                                                                        $$ = createNode(op_node, "VarParams", OP);
                                                                                        addChild($$, $2);
                                                                                        aux = createNode(id_node, "ID", $4);
                                                                                        addChild($$,aux);
                                                                                    }
;

funcBlock: varPart statPart                                                         {
                                                                                        $$ = createNode(op_node, "VarPart", OP);
                                                                                        addChild($$, $1);
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $2);
                                                                                                addSibling($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addSibling($$, $2);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addSibling($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                    }
;

statPart: compStat                                                                  {
                                                                                        if($1 != NULL)
                                                                                        {
                                                                                            if($1->right != NULL)
                                                                                            {
                                                                                                $$ = createNode(op_node, "StatList", OP);
                                                                                                addChild($$, $1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $$ = $1;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = NULL;
                                                                                        }
                                                                                    }
;

compStat: BEGINSYM statList ENDSYM                                                  {
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                $$ = createNode(op_node, "StatList", OP);
                                                                                                addChild($$, $2);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $$ = $2;
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = NULL;
                                                                                        }
                                                                                    }
;

statList: stat optStatList                                                          {
                                                                                        if($1 != NULL)
                                                                                        {
                                                                                            $$ = $1;
                                                                                            addSibling($$, $2);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = $2;
                                                                                        }
                                                                                    }
;

optStatList:                                                                        {
                                                                                        $$ = NULL;
                                                                                    }
           | SEMICSYM stat optStatList                                              {
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            $$ = $2;
                                                                                            addSibling($$, $3);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $$ = $3;
                                                                                        }
                                                                                    }
;

stat: compStat                                                                      {
                                                                                        $$ = $1;
                                                                                    }
    | IFSYM expr THENSYM stat                                                       {
                                                                                        $$ = createNode(op_node, "IfElse", OP);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                        aux = createNode(op_node, "StatList", OP);
                                                                                        addChild($$,aux);
                                                                                    }
    | IFSYM expr THENSYM stat ELSESYM stat                                          {
                                                                                        $$ = createNode(op_node, "IfElse", OP);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                        if($6 != NULL)
                                                                                        {
                                                                                            if($6->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $6);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $6);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                    }
    | WHILESYM expr DOSYM stat                                                      {
                                                                                        $$ = createNode(op_node, "While", OP);
                                                                                        addChild($$, $2);
                                                                                        if($4 != NULL)
                                                                                        {
                                                                                            if($4->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $4);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $4);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                    }
    | REPEATSYM statList UNTILSYM expr                                              {
                                                                                        $$ = createNode(op_node, "Repeat", OP);
                                                                                        if($2 != NULL)
                                                                                        {
                                                                                            if($2->right != NULL)
                                                                                            {
                                                                                                aux = createNode(op_node, "StatList", OP);
                                                                                                addChild(aux, $2);
                                                                                                addChild($$, aux);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                addChild($$, $2);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            addChild($$, createNode(op_node, "StatList", OP));
                                                                                        }
                                                                                        addChild($$, $4);
                                                                                    }
    | VALSYM LBRACSYM PARAMSTRSYM LBRACSYM expr RBRACSYM COMMASYM ID RBRACSYM       {
                                                                                        $$ = createNode(op_node, "ValParam", OP);
                                                                                        addChild($$, $5);
                                                                                        addChild($$, createNode(id_node, "ID", $8));
                                                                                    }
    | ID ASSIGNSYM expr                                                             {
                                                                                        $$ = createNode(op_node, "Assign", OP);
                                                                                        addChild($$, createNode(id_node, "ID", $1));
                                                                                        addChild($$, $3);
                                                                                    }
    |                                                                               {
                                                                                        $$ = NULL;
                                                                                    }
    | WRITELNSYM writelnPList                                                       {
                                                                                        $$ = createNode(op_node, "WriteLn", OP);
                                                                                        addChild($$, $2);
                                                                                    }
    | WRITELNSYM                                                                    {
                                                                                        $$ = createNode(op_node, "WriteLn", OP);
                                                                                    }
;

writelnPList: LBRACSYM expr optWritelnPList RBRACSYM                                {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                    }
            | LBRACSYM STRING optWritelnPList RBRACSYM                              {
                                                                                        $$ = createNode(string_node, "String", $2);
                                                                                        addSibling($$, $3);
                                                                                    }
;

optWritelnPList:                                                                    {
                                                                                        $$ = NULL;
                                                                                    }
               | COMMASYM expr optWritelnPList                                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                    }
               | COMMASYM STRING optWritelnPList                                    {
                                                                                        $$ = createNode(string_node, "String", $2);
                                                                                        addSibling($$, $3);
                                                                                    }
;

expr: simpleExpr                                                                    {
                                                                                        $$ = $1;
                                                                                    }
    | simpleExpr LT simpleExpr                                                      {
                                                                                        $$ = createNode(op_node, "Lt", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr GT simpleExpr                                                      {
                                                                                        $$ = createNode(op_node, "Gt", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }

    | simpleExpr EQ simpleExpr                                                      {
                                                                                        $$ = createNode(op_node, "Eq", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr NEQ simpleExpr                                                     {
                                                                                        $$ = createNode(op_node, "Neq", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr LEQ simpleExpr                                                     {
                                                                                        $$ = createNode(op_node, "Leq", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | simpleExpr GEQ simpleExpr                                                     {
                                                                                        $$ = createNode(op_node, "Geq", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
;

simpleExpr: simpleExpr PLUS term                                                    {
                                                                                        $$ = createNode(op_node, "Add", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | simpleExpr MINUS term                                                   {
                                                                                        $$ = createNode(op_node, "Sub", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | simpleExpr OR term                                                      {
                                                                                        $$ = createNode(op_node, "Or", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
          | PLUS term                                                               {
                                                                                        $$ = createNode(op_node, "Plus", OP);
                                                                                        addChild($$, $2);
                                                                                    }
          | MINUS term                                                              {
                                                                                        $$ = createNode(op_node, "Minus", OP);
                                                                                        addChild($$, $2);
                                                                                    }
          | term                                                                    {
                                                                                        $$ = $1;
                                                                                    }
;

term: term MUL term                                                                 {
                                                                                        $$ = createNode(op_node, "Mul", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term REALDIV term                                                             {
                                                                                        $$ = createNode(op_node, "RealDiv", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term DIV term                                                                 {
                                                                                        $$ = createNode(op_node, "Div", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term MOD term                                                                 {
                                                                                        $$ = createNode(op_node, "Mod", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | term AND term                                                                 {
                                                                                        $$ = createNode(op_node, "And", OP);
                                                                                        addChild($$, $1);
                                                                                        addChild($$, $3);
                                                                                    }
    | factor                                                                        {
                                                                                        $$ = $1;
                                                                                    }
;

factor: ID                                                                          {
                                                                                        $$ = createNode(id_node, "ID", $1);
                                                                                    }
      | INTLIT                                                                      {
                                                                                        $$ = createNode(intLit_node, "IntLit", $1);
                                                                                    }
      | REALLIT                                                                     {
                                                                                        $$ = createNode(realLit_node, "RealLit", $1);
                                                                                    }
      | functionDesignator                                                          {
                                                                                        $$ = $1;
                                                                                    }
      | LBRACSYM expr RBRACSYM                                                      {
                                                                                        $$ = $2;
                                                                                    }
      | NOTSYM factor                                                               {
                                                                                        $$ = createNode(op_node, "Not", OP);
                                                                                        addChild($$, $2);
                                                                                    }
;

functionDesignator: ID paramList                                                    {
                                                                                        $$ = createNode(op_node, "Call", OP);
                                                                                        addChild($$, createNode(id_node, "ID", $1));
                                                                                        addChild($$, $2);
                                                                                    }
;

paramList: LBRACSYM expr optParamList RBRACSYM                                      {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                    }
;

optParamList:                                                                       {
                                                                                        $$ = NULL;
                                                                                    }
            | COMMASYM expr optParamList                                            {
                                                                                        $$ = $2;
                                                                                        addSibling($$, $3);
                                                                                    }
;

%%
int main(int argc, char* argv[])
{
    if(argc!=1)
    {
        if(strcmp(argv[1],"-t")==0)
        {
            printFlag = 1;
        }
    }
    yyparse();
    if(printFlag)
    {
        printTree(root,0);
    }
    clearTree(root);
    return 0;
}

void yyerror(char* s)
{
    printf("Line %d, col %lu: %s: %s\n", yylineno, (unsigned long)(column-strlen(yytext)), s, yytext);
}
