#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"

node* createNode(node_type_t nodeType, char* nodeTypeString, char* variable)
{
    node* result = (node*)malloc(sizeof(node));
    result->nodeType = nodeType;
    result->nodeTypeString = (char*)malloc(1+strlen(nodeTypeString)*sizeof(char));
    strcpy(result->nodeTypeString, nodeTypeString);
    result->var = variable;
    result->children = 0;
    result->nChildren = 0;
    result->parent = 0;
    result->left = 0;
    result->right = 0;
    return result;
}

void addChild(node* parent, node* child)
{
    if(parent==0 || child==0)
    {
        return;
    }
    parent->nChildren++;
    parent->children = (node**)realloc(parent->children, parent->nChildren*sizeof(node*));
    parent->children[parent->nChildren-1] = child;
    child->parent = parent;
    node* aux = child;
    while((aux = aux->right) != 0)
    {
        parent->nChildren++;
        parent->children = (node**)realloc(parent->children, parent->nChildren*sizeof(node*));
        parent->children[parent->nChildren-1] = aux;
        aux->parent = parent;
    }
}

void addSibling(node* left, node* right)
{
    if(left==0 || right==0)
    {
        return;
    }
    node* aux = left;
    while(aux->right!=0)
    {
        aux = aux->right;
    }
    aux->right = right;
    right->left = aux;
    if(left->parent!=0)
    {
        addChild(left->parent, right);
    }
}

void printTree(node* current, int level)
{
    if(current==0)
    {
        return;
    }
    int i;
    for(i=0; i<level; i++)
    {
        printf("..");
    }
    if(current->nodeType == id_node)
    {
        printf("Id(%s)\n",current->var);
    }
    else if(current->nodeType == intLit_node)
    {
        printf("IntLit(%s)\n", current->var);
    }
    else if(current->nodeType == realLit_node)
    {
        printf("RealLit(%s)\n", current->var);
    }
    else if(current->nodeType == string_node)
    {
        printf("String(%s)\n", current->var);
    }
    else
    {
        printf("%s\n", current->nodeTypeString);
    }
    for(i=0; i<current->nChildren; i++)
    {
        printTree(current->children[i], level+1);
    }
}

void clearTree(node* current)
{
    if(current == 0)
    {
        return;
    }
    int i;
    for(i=0; i<current->nChildren; i++)
    {
        clearTree(current->children[i]);
    }
    free(current->children);
    free(current->nodeTypeString);
    free(current->var);
    free(current);
}
