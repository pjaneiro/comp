	.text
	.file	"helloworld.ll"
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp0:
	.cfi_def_cfa_offset 16
	movl	$msg, %edi
	callq	puts
	xorl	%eax, %eax
	popq	%rdx
	retq
.Ltmp1:
	.size	main, .Ltmp1-main
	.cfi_endproc

	.type	msg,@object             # @msg
	.section	.rodata,"a",@progbits
msg:
	.asciz	"Hello World!"
	.size	msg, 13


	.section	".note.GNU-stack","",@progbits
