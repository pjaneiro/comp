# Lex

flex calc.l

if [ -f "lex.yy.c" ]
then
  echo "Lex is installed."
else
  echo "Lex is not installed!"
fi



# Yacc

yacc -d calc.y

if [ -f "y.tab.h" ]
then
  echo "Yacc/Bison is installed."
else
  echo "Yacc/Bison is not installed!"
fi



# LLVM

llc -o=middle.s helloworld.ll
gcc -o helloworld middle.s

if [ -f "middle.s" ]
then
  echo "LLVM is installed."
else
  echo "LLVM is not installed!"
fi

./helloworld

if [ $? -ne 0 ]
then
    echo "The LLVM Hello World example did not execute successfully."
fi
