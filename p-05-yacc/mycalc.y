%{
#include<stdio.h>
%}
%token INT DOUBLE VARIABLE END MINUS PLUS LBRAC RBRAC EQUAL DIV MUL ENDL

%union
{
    int valInt;
    double valDouble;
    char* variable;
}

%type <valInt> intExpression;
%type <valDouble> doubleExpression;
%type <valInt> INT;
%type <valDouble> DOUBLE;
%type <variable> VARIABLE;

%nonassoc MINUS PLUS
%nonassoc MUL DIV
%nonassoc LBRAC RBRAC

%%

calc: calc END ENDL               {return 0;}
    | calc intExpression ENDL     {printf("%d\n", $2);}
    | calc doubleExpression ENDL  {printf("%f\n", $2);}
    |
    ;

intExpression: intExpression PLUS intExpression     {$$ = $1 + $3;}
             | intExpression MINUS intExpression    {$$ = $1 - $3;}
             | intExpression MUL intExpression      {$$ = $1 * $3;}
             | intExpression DIV intExpression      {$$ = $1 / $3;}
             | INT                                  {$$ = $1;}
             ;

doubleExpression: doubleExpression PLUS doubleExpression    {$$ = $1 + $3;}
                | doubleExpression MINUS doubleExpression   {$$ = $1 - $3;}
                | doubleExpression MUL doubleExpression     {$$ = $1 * $3;}
                | doubleExpression DIV doubleExpression     {$$ = $1 / $3;}
                | DOUBLE                                    {$$ = $1;}
                ;

%%

int yyerror(char* s)
{
    fprintf(stderr, "%s\n", s);
    return 0;
}

int main()
{
    yyparse();
    return 0;
}
