%{
#include "y.tab.h"
%}

%%

end             return END;

\-              return MINUS;
\+              return PLUS;
\(              return LBRAC;
\)              return RBRAC;
\=              return EQUAL;
\/              return DIV;
\*              return MUL;
\n              return ENDL;
[ \t]           ;                       /*Ignorar espaço e tab*/

[0-9]+          {yylval.valInt=atoi(yytext); return INT;}
[0-9]+\.[0-9]+  {yylval.valDouble=atof(yytext); return DOUBLE;}
[A-z][A-z0-9_]* {yylval.variable=(char*)calloc(1+yyleng,sizeof(char)); strncpy(yylval.variable,yytext, yyleng); return VARIABLE;}


.               return yytext[0];

%%

int yywrap()
{
    return 1;
}
