%{
#include <stdio.h>
#define ATRASO 3
%}
time						[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}

%%

{time}						{
	int hour, minute, second, mili, time;
	sscanf(yytext, "%d:%d:%d,%d",&hour,&minute,&second,&mili);
	time = 3600*hour+60*minute+second+ATRASO;
	hour = time/3600;
	minute = (time-hour*3600)/60;
	second = (time-hour*3600-minute*60);
	printf("%02d:%02d:%02d,%03d",hour,minute,second,mili);
}

%%

int main()
{
	yylex();
	printf("\n");
	return 0;
}

int yywrap()
{
	return 1;
}