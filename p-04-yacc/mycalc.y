%{
#include<stdio.h>
%}
%left '+' '-'
%left '*' '/'
%left '('
%token NUMBER END

%%

calc: 		calc END '\n'					{return 0;}
			|	calc expression '\n'		{printf("%d\n", $2);}
			|
			;

expression:	expression '+' expression   	{$$=$1+$3;}
			| 	expression '-' expression  	{$$=$1-$3;}
			| 	expression '/' expression  	{
												if($3==0)
												{
													yyerror("Divide by zero: ");
												}
												else
												{
													$$=$1/$3;
												}
											}
			| 	expression '*' expression  	{$$=$1*$3;}
			|	'(' expression ')'			{$$=$2;}
			| 	NUMBER						{$$=$1;}
			|	'-' NUMBER					{$$=0-$2;}
			;

%%

int yyerror(char* s)
{
	fprintf(stderr, "%s\n", s);
	return 0;
}

int main()
{
	yyparse();
	return 0;
}
