lisper: y.tab.c lex.yy.c
	gcc -o lisper y.tab.c lex.yy.c -ll -ly

y.tab.c: lisper.y
	yacc -d lisper.y

lex.yy.c: lisper.l
	lex lisper.l
