%{
#include "y.tab.h"
extern int yylval;
%}

%%

[0-9]+(\.[0-9]+)?	{yylval=atoi(yytext);
					return NUMBER;}

[-+/\*]				return OPERATOR;

\(					return LBRAC;

\)					return RBRAC;

[ \t]				;

[.\n]				return *yytext;

%%

int yywrap()
{
	return 1;
}