%{
#include<stdio.h>
void yyerror(char*);
%}
%left '+' '-' '*' '/'
%left '('
%token NUMBER OPERATOR LBRAC RBRAC

%%

lisp:		lisp expression	'\n'						{printf("Correto\n");}
			|	lisp error '\n'							{printf("Incorreto\n");}
			|
			;

expression:	LBRAC OPERATOR expression expression RBRAC
			|	NUMBER
			;

%%

void yyerror(char* s)
{
	return;
}

int main()
{
	yyparse();
	return 0;
}
