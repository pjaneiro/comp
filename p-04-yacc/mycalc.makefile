mycalc: y.tab.c lex.yy.c
	gcc -o mycalc y.tab.c lex.yy.c -ll -ly

y.tab.c: mycalc.y
	yacc -d mycalc.y

lex.yy.c: mycalc.l
	lex mycalc.l
