numero														[0-9]+
separador													[","|"."]
sinal														"+"|"-"
%%
%{
/*
"-"{numero}													{printf(" Inteiro negativo ");}
"+"?{numero}												{printf(" Inteiro positivo ");}
"-"{numero}"."{numero}										{printf(" Real negativo ");}
"+"?{numero}"."{numero}										{printf(" Real positivo ");}
*/
%}
(abc|ab+c|a?(bc)+)											{printf(" token! ");}
{sinal}?{numero}											{printf(" integer ");}
{sinal}?{numero}l											{printf(" long ");}
{sinal}?{numero}u											{printf(" unsigned int ");}
{sinal}?{numero}lu											{printf(" unsigned long ");}
{sinal}?{numero}f											{printf(" float ");}
{sinal}?{numero}{separador}{numero}							{printf(" float ");}
{sinal}?{numero}({separador}{numero})?e{sinal}{numero}		{printf(" floating point ");}

%%
int main()
{
	yylex();
	return 0;
}

int yywrap()
{
	return 1;
}

